import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';

import { LeafletModule } from '@asymmetrik/ngx-leaflet';

declare let L;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

@NgModule({
  imports: [LeafletModule]
})

export class AppComponent implements OnInit {
  title = 'Project Hospitals';
  constructor() { }

  ngOnInit() {
  }
}











