import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import * as schoolData from './bc.json';
import * as hospitalData from './new 1.json';

declare let L;

@NgModule({
  imports: [FormsModule]
})

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.css']
})

export class MapsComponent implements OnInit {

  myIcon: any = L.icon({
    iconUrl: './assets/leaflet/images/hospitalmarker.png',

    iconSize: [25, 41], // size of the icon
    iconAnchor: [12, 41],
    popupAnchor: [1, -30]
  });

  areas: string;
  schoolsCount: any;
  map: any;
  schoolMarkerArray: Array<Object> = [];
  hospitalMarkerArray: Array<Object> = [];
  
  onEnterKey(event: any) {
    let schoolMarker;
    this.areas = event.target.value; 

    for (let i = 0; i < this.schoolMarkerArray.length; i++) {
      this.map.removeLayer(this.schoolMarkerArray[i]);
    }
    this.schoolMarkerArray = [];

    for (let j = 0; j < this.hospitalMarkerArray.length; j++) {
      this.map.removeLayer(this.hospitalMarkerArray[j]);
    }
    this.hospitalMarkerArray = [];

    this.schoolsCount = "";

    if (this.areas && event.which === 13) {
      let val = Object.values(schoolData)[0];

     for (let i = 0; i < schoolData.length; i++) {
        if (val[i].address.includes(this.areas)) {
          schoolMarker = (L.marker([val[i].latitude, val[i].longitude]).addTo(this.map));
          schoolMarker.bindPopup(`${val[i].name}`).openPopup();
          this.schoolMarkerArray.push(schoolMarker);
        }
      }
      this.schoolsCount = this.schoolMarkerArray.length;
    }
  }

  onButtonPress(){
    let hospitalMarker;
    if (this.areas) {
      let hospitalVal = Object.values(hospitalData)[0];

      for (let j = 0; j < hospitalData.length; j++) {
        if (hospitalVal[j].address.includes(this.areas)) {
          hospitalMarker = (L.marker([hospitalVal[j].latitude, hospitalVal[j].longitude], { icon: this.myIcon }).addTo(this.map));
          hospitalMarker.bindPopup(`${hospitalVal[j].name}`).openPopup();
          this.hospitalMarkerArray.push(hospitalMarker);
        }
      }
    }
  }
  ngOnInit() {
    this.map = L.map('mapid').setView([13.116635, 80.097287], 13);
    const mapLayer = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(this.map);

    this.schoolsCount = "";
  }
}
