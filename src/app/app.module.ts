import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxChartsModule } from "@swimlane/ngx-charts";

import { AppComponent } from './app.component';
import { MapsComponent } from './maps/maps.component';

import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ChartsComponent } from './charts/charts.component';

@NgModule({
  declarations: [
    AppComponent,
    MapsComponent,
    ChartsComponent,
  ],
  imports: [
    BrowserModule,
    NgxChartsModule,
    LeafletModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
